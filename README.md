# Reference decoder
## Build
```
cd hevc-ref-decoder
chmod +x build-ref-decoder.sh
./build-ref-decoder.sh
```

## Usage
In the `HM-HM-16.22/bin` folder execute `TAppDecoderStatic` and redirect stdout to your
favorite location.
```
./TAppDecoderStatic -b <HEVC file> > logfile.log
```

# Log parser
## Build
```
mkdir hevc-ref-parser/build
cd hevc-ref-parser/build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j 
```

## Usage
```
./generate_byteranges <log file> [--references <path/to/references>] [--frame-headers <path/to/frames>] [--segment-size <segment-size>]
```
* `<log file>` is the output from the reference decoder.
* `--references` specifies an output path for the reference graph files. These
files contain the reference structure of a segment. Each line corresponds to
one edge in the graph, represented by the POC of the frame that is referenced,
the number of coding units that are referenced, and the POC of the frame from
which the reference originates.
* `--frame-headers` specifies an output path for the frame header files. These
files contain the frames of a segment in bytestream order. For each frame the
POC, type, and header size in bytes is specified.

The target folder for `--references` and `--frame-headers` must already exist.
Inside the folder a new structure with the pattern `<tile#>/<segment#>.dat` is
created.