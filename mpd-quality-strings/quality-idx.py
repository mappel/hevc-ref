#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import os
import sys


def parse_ranges(ranges: str) -> (int, int):
    """Parse a comma-separated list of byte ranges and return the number of
    ranges as well as their summed size as a pair (number, size)."""
    range_count = 0
    range_size = 0
    ranges_split = ranges.split(',')
    for range in ranges_split:
        range_split = range.split('-')
        if len(range_split) != 2:
            print('Error: Invalid byterange:', range, file=sys.stderr)
            return -1, -1
        start = int(range_split[0])
        end = int(range_split[1])
        range_count += 1
        range_size += end - start + 1
    return range_count, range_size


def modify_segmenturl(line: str, quality: int) -> str:
    indent_size = len(line) - len(line.lstrip())
    # Because we can not be sure what the last attribute is, which will have
    # '/>' included since there is no space, we will remove it in the beginning
    # and add it in the end.
    temp_line = line.strip().rstrip('/>')
    line_split = temp_line.split()
    media_range_pos = -1
    quality_string_pos = -1
    unreliable_ranges_pos = -1
    for idx, attribute in enumerate(line_split):
        if attribute.startswith('mediaRange'):
            media_range_pos = idx + 1
        elif attribute.startswith('quality'):
            quality_string_pos = idx
        elif attribute.startswith('unreliable'):
            unreliable_ranges_pos = idx
    if unreliable_ranges_pos == -1:
        print('Error: Failed to find unreliable range attribute in line:', line.strip(), file=sys.stderr)
        return line
    unreliable_ranges_split = line_split[unreliable_ranges_pos].split('=')
    if len(unreliable_ranges_split) != 2:
        print('Error: Malformed unreliable range attribute in line:', line.strip(), file=sys.stderr)
        return line
    range_count, range_size = parse_ranges(unreliable_ranges_split[1].strip('"'))
    if range_count < 0:
        print('in line:', line.strip(), file=sys.stderr)
        return line
    quality_string = 'quality="{quality}:{range_count}:{range_size}"'\
        .format(quality=quality, range_count=range_count, range_size=range_size)
    if quality_string_pos != -1:
        print('Warning: Replacing existing quality string.', file=sys.stderr)
        line_split[quality_string_pos] = quality_string
    elif media_range_pos == -1:
        print('Warning: Failed to find mediaRange attribute. Inserting at end', file=sys.stderr)
        line_split.append(quality_string)
    else:
        line_split.insert(media_range_pos, quality_string)
    ret_line = ' ' * indent_size + ' '.join(line_split) + '/>\n'
    return ret_line


def modify_mpd(mpd: str, output: str) -> None:
    # curr_track == 0 means base track, which we skip.
    curr_track = -1
    curr_quality = 0
    with open(mpd, 'r') as i, open(output, 'w') as o:
        for line in i:
            if '<AdaptationSet' in line:
                curr_track += 1
                curr_quality = 0
            elif '<Representation' in line:
                curr_quality += 1
            elif 'SegmentURL' in line and curr_track > 0:
                line = modify_segmenturl(line, curr_quality)
            o.write(line)


if __name__ == '__main__':
    description = """Add quality strings according to the quality index order to the MPD.
                  This order always requests full tiles and specifies the quality as the index of the quality level,
                  starting with the lowest quality as 1."""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('mpd', metavar='MPD', help='the MPD file')
    parser.add_argument('-o', '--output', help='set the output MPD manually')
    args = parser.parse_args()

    output = args.output
    if output is None:
        output = os.path.splitext(args.mpd)[0] + '_output.mpd'
    modify_mpd(args.mpd, output)
    exit(0)
