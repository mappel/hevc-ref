#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import os
import sys

def parse_ranges(ranges: str) -> (int, int):
    """Parse a comma-separated list of byte ranges and return the number of
    ranges as well as their summed size as a pair (number, size)."""
    range_count = 0
    range_size = 0
    ranges_split = ranges.split(',')
    for range in ranges_split:
        range_split = range.split('-')
        if len(range_split) != 2:
            print('Error: Invalid byterange:', range, file=sys.stderr)
            return -1, -1
        start = int(range_split[0])
        end = int(range_split[1])
        range_count += 1
        range_size += end - start + 1
    return range_count, range_size


def get_crf_from_baseurl(baseurl: str) -> int:
    value = baseurl.strip()[len('<BaseURL>'):-len('</BaseURL>')]
    split = value.split('_')
    for entry in split:
        if entry.isdigit():
            print('BaseURL:', value, 'assuming CRF:', entry)
            return int(entry)
    print('Error: Failed to get CRF from BaseURL:', value, file=sys.stderr)
    return -1


def modify_segmenturl(line: str, ssim: str) -> str:
    indent_size = len(line) - len(line.lstrip())
    # Because we can not be sure what the last attribute is, which will have
    # '/>' included since there is no space, we will remove it in the beginning
    # and add it in the end.
    temp_line = line.strip().rstrip('/>')
    line_split = temp_line.split()
    media_range_pos = -1
    quality_string_pos = -1
    unreliable_ranges_pos = -1
    for idx, attribute in enumerate(line_split):
        if attribute.startswith('mediaRange'):
            media_range_pos = idx + 1
        elif attribute.startswith('quality'):
            quality_string_pos = idx
        elif attribute.startswith('unreliable'):
            unreliable_ranges_pos = idx
    if unreliable_ranges_pos == -1:
        print('Error: Failed to find unreliable range attribute in line:', line.strip(), file=sys.stderr)
        return line
    unreliable_ranges_split = line_split[unreliable_ranges_pos].split('=')
    if len(unreliable_ranges_split) != 2:
        print('Error: Malformed unreliable range attribute in line:', line.strip(), file=sys.stderr)
        return line
    range_count, range_size = parse_ranges(unreliable_ranges_split[1].strip('"'))
    if range_count < 0:
        print('in line:', line.strip(), file=sys.stderr)
        return line
    quality_string = 'quality="{quality}:{range_count}:{range_size}"' \
        .format(quality=ssim, range_count=range_count, range_size=range_size)
    if quality_string_pos != -1:
        print('Warning: Replacing existing quality string.', file=sys.stderr)
        line_split[quality_string_pos] = quality_string
    elif media_range_pos == -1:
        print('Warning: Failed to find mediaRange attribute. Inserting at end', file=sys.stderr)
        line_split.append(quality_string)
    else:
        line_split.insert(media_range_pos, quality_string)
    ret_line = ' ' * indent_size + ' '.join(line_split) + '/>\n'
    return ret_line


def modify_mpd(mpd: str, ssims: dict, output: str) -> None:
    # curr_track == 0 means base track, which we skip.
    curr_track = -1
    curr_crf = -1
    ref_crf = -1
    curr_segment_no = -1
    error = False
    with open(mpd, 'r') as i, open(output, 'w') as o:
        for line in i:
            if '<AdaptationSet' in line:
                curr_track += 1
            elif 'SegmentURL' in line and curr_track > 0:
                ssim = 1.0
                if curr_crf in ssims:
                    if curr_segment_no not in ssims[curr_crf]:
                        print('Error: Failed to find entry for segment:', curr_segment_no, 'quality:', curr_crf,
                              file=sys.stderr)
                        error = True
                        break
                    if curr_track not in ssims[curr_crf][curr_segment_no]:
                        print('Error: Failed to find entry for tile:', curr_track, 'segment:', curr_segment_no,
                              'quality:', curr_crf, file=sys.stderr)
                        error = True
                        break
                    ssim = ssims[curr_crf][curr_segment_no][curr_track]
                line = modify_segmenturl(line, ssim)
                curr_segment_no += 1
            elif 'BaseURL' in line:
                curr_segment_no = 1
                curr_crf = get_crf_from_baseurl(line)
                if curr_crf < 0:
                    error = True
                    break
                if curr_crf not in ssims:
                    if ref_crf == -1:
                        ref_crf = curr_crf
                        print('Assuming reference CRF:', ref_crf)
                    elif curr_crf != ref_crf:
                        print('Error: More than one CRF missing from SSIM file. First:', ref_crf, 'Second:', curr_crf,
                              file=sys.stderr)
                        error = True
                        break
            o.write(line)
    if error and os.path.exists(output):
        os.remove(output)


def parse_ssims(ssims: str) -> dict:
    """Parse an SSIM file into a dictionary.

    Expected format for input is a space-separated file with the line format:
        crf segment_no ssim_tile_1 ssim_tile_2 ...
    At least one tile needs to be specified.

    Return a nested dictionary mapping crf -> segment_no -> tile_no -> ssim."""
    ret = dict()
    with open(ssims, 'r') as f:
        headers = f.readline()
        header_split = headers.split()
        if len(header_split) < 3:
            print('Error: No tiles found, or file has invalid format (expect at least three columns).', file=sys.stderr)
            return dict()
        num_tiles = len(header_split) - 2
        for line in f:
            line_split = line.split()
            num_tiles_in_line = len(line_split) - 2
            if num_tiles_in_line != num_tiles:
                print('Error: Number of tiles in line does not match header (header:', num_tiles, 'line:',
                      num_tiles_in_line, '):', line.strip(), file=sys.stderr)
                return dict()
            crf = int(line_split[0])
            if crf not in ret:
                ret[crf] = dict()
            segment_no = int(line_split[1])
            if segment_no not in ret[crf]:
                ret[crf][segment_no] = dict()
            for tile_idx, ssim in enumerate(line_split[2:]):
                ret[crf][segment_no][tile_idx + 1] = ssim
    # Sanity checks
    if len(ret) == 0:
        print('Error: No lines found.', file=sys.stderr)
        return dict()
    crfs = list(ret.keys())
    ref_segment_count = len(ret[crfs[0]])
    for crf in crfs[1:]:
        if len(ret[crf]) != ref_segment_count:
            print('Error: Number of segments do not match for qualities', crfs[0], 'and', crf, file=sys.stderr)
            return dict()
    return ret


if __name__ == '__main__':
    description = """Add quality strings according to the single SSIM order to the MPD.
                  
                  This order always requests full tiles and specifies the quality as a single SSIM score, calculated for
                  each segment of each tile using the highest quality as the reference.
                  
                  The SSIM file should contain entries for all qualities except the reference, since this one always has
                  a perfect SSIM score. The script automatically detects which quality is missing and assumes that this
                  is the reference. If more than one quality is missing, an error is raised."""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('mpd', metavar='MPD', help='the MPD file')
    parser.add_argument('ssims', help='the file containing SSIM information')
    parser.add_argument('-o', '--output', help='set the output MPD manually')
    args = parser.parse_args()

    output = args.output
    if output is None:
        output = os.path.splitext(args.mpd)[0] + '_output.mpd'
    ssim_dict = parse_ssims(args.ssims)
    if len(ssim_dict) == 0:
        exit(1)
    modify_mpd(args.mpd, ssim_dict, output)
    exit(0)
