#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
from collections import namedtuple
import sys
import os
from pathlib import Path

FRAME_FILE_ENDING = '.dat'
SLICE_DATA_FILE_ENDING = '.range'
OUTPUT_FILE_ENDING = '.range'

Frame = namedtuple('Frame', 'idx poc weight')
Range = namedtuple('Range', 'start end')

# MEMO: Both the frame files and the slice data range files are in bytestream
# order. Therefore, if we read these files into lists, the list indices are the
# same. In a second step, we optionally apply weights to the frames and want to
# sort them accordingly. Since this messes up the list order, we need to
# explicitly store the original list index as well. Then, we can sort the list
# by weights, iterate it in this order, and still write the correct range for
# each frame.


def read_frame_file(frame_file: str) -> list:
    """Read a single frame file and return the contents as a list that contains
    one entry per frame.

    Assumes that the frame file contains a header line and that the first frame
    line is an I-frame, which is excluded from the returned list.
    Required line format:
        poc:int [...]
    e.g.
        16 I 4
    Only the first column is used and no column-count check is performed.
    """
    ret = list()
    with open(frame_file, 'r') as f:
        # Consume headers
        f.readline()
        # Skip I-frame
        f.readline()
        for idx, line in enumerate(f):
            line_split = line.split()
            try:
                poc = int(line_split[0])
            except ValueError as e:
                print('Error: Invalid POC in line', idx + 1, 'of frame file:',
                      frame_file, file=sys.stderr)
                print('Line:', line.strip(), file=sys.stderr)
                print('Exception:', e, sys.stderr)
                return list()
            ret.append(Frame(idx, poc, 0))  # Default weight of 0
    return ret


def read_frame_order(frame_path: str) -> dict:
    """Read all frame orders from the specified path.
    Returns a two-tiered dictionary mapping tile_no -> segment_no -> frame.

    Expects one folder per tile, where the folder name is the tile number.
    Inside each tile folder, one file per segment with the naming scheme
    segment_no.FRAME_FILE_ENDING is expected.
    """
    ret = dict()
    for entry in os.scandir(frame_path):
        if not entry.is_dir():
            continue
        try:
            tile_no = int(entry.name)
        except ValueError:
            print('Warning: Skipping possible tile directory:',
                  frame_path + entry.name, file=sys.stderr)
            continue
        ret[tile_no] = dict()
        tile_dir = frame_path + entry.name + '/'
        for subentry in os.scandir(tile_dir):
            if subentry.is_file() and subentry.name.endswith(FRAME_FILE_ENDING):
                try:
                    segment_no = int(subentry.name[:-len(FRAME_FILE_ENDING)])
                except ValueError:
                    print('Warning: Skipping possible segment entry:',
                          tile_dir + subentry.name, file=sys.stderr)
                    continue
                frame_order = read_frame_file(tile_dir + subentry.name)
                if len(frame_order) == 0:
                    return dict()
                ret[tile_no][segment_no] = frame_order
    return ret


def read_range_file(range_file: str) -> list:
    """Read a single slice data range file and return the contents as a list
    that contains one entry per range.

    Required line format:
        start:int end:int
    e.g.
        0 42
    """
    ret = list()
    with open(range_file, 'r') as f:
        for idx, line in enumerate(f):
            line_split = line.split()
            if len(line_split) != 2:
                print('Error: Range line has unexpected format in line',
                      idx + 1, 'of range file', range_file, file=sys.stderr)
                print('Line:', line.strip(), file=sys.stderr)
                return list()
            try:
                start = int(line_split[0])
                end = int(line_split[1])
            except ValueError as e:
                print('Error: Invalid range in line', idx + 1, 'of range file:',
                      range_file, file=sys.stderr)
                print('Line:', line.strip(), file=sys.stderr)
                print('Exception:', e, sys.stderr)
                return list()
            ret.append(Range(start, end))
    return ret


def read_slice_data_ranges(slice_data_path: str) -> dict:
    """Read all slice data ranges from the specified path.
    Returns a two-tiered dictionary mapping tile_no -> segment_no -> ranges.

    Expects one folder per tile, where the folder name is the tile number.
    Inside each tile folder, one file per segment with the naming scheme
    segment_no.SLICE_DATA_FILE_ENDING is expected.
    """
    ret = dict()
    for entry in os.scandir(slice_data_path):
        if not entry.is_dir():
            continue
        try:
            tile_no = int(entry.name)
        except ValueError:
            print('Warning: Skipping possible tile directory:',
                  slice_data_path + entry.name, file=sys.stderr)
            continue
        ret[tile_no] = dict()
        tile_dir = slice_data_path + entry.name + '/'
        for subentry in os.scandir(tile_dir):
            if subentry.is_file() \
                    and subentry.name.endswith(SLICE_DATA_FILE_ENDING):
                try:
                    segment_no = \
                        int(subentry.name[:-len(SLICE_DATA_FILE_ENDING)])
                except ValueError:
                    print('Warning: Skipping possible segment entry:',
                          tile_dir + subentry.name, file=sys.stderr)
                    continue
                slice_data_ranges = read_range_file(tile_dir + subentry.name)
                if len(slice_data_ranges) == 0:
                    return dict()
                ret[tile_no][segment_no] = slice_data_ranges
    return ret


def write_range_files(output_path: str, frame_order: list,
                      slice_data_ranges: list) -> None:
    # Sort frame order according to weights. The list is iterated in reverse
    # order, i.e., the least important frames (with the lowest weight), which
    # are dropped first, should be at the back.
    frame_order.sort(key=lambda t: t[2], reverse=True)
    # Reverse order for iteration -> Least important frames are at the front.
    frame_order.reverse()
    removed_frames = list()
    # Create an empty file with no dropped frames, which is still a valid case
    # since we have SSIM degradation due to quality differences independent from
    # dropped frames.
    Path(output_path + '0' + OUTPUT_FILE_ENDING).touch()
    for frame in frame_order:
        removed_range = slice_data_ranges[frame.idx]
        removed_frames.append(str(removed_range.start) + ' ' +
                              str(removed_range.end) + '\n')
        output_file = output_path + str(len(removed_frames)) + \
                      OUTPUT_FILE_ENDING
        with open(output_file, 'w') as f:
            f.writelines(removed_frames)


def write_output(output_path: str, frame_order: dict, slice_data_ranges: dict) \
        -> int:
    """Write the incremental range files to the output directory.

    Creates a folder structure in the output directory with the following
    format: output/tile_no/segment_no/
    Within each segment folder, one file per number-of-dropped-frames with the
    format number-of-dropped-frames.OUTPUT_FILE_ENDING is created.
    The frames are dropped in the order specified by their weights. The frame
    with the highest weight is dropped last, i.e., it is only contained in one
    file (where all frames but the I-frame are dropped)."""
    if frame_order.keys() != slice_data_ranges.keys():
        print('Error: Tile numbers do not match for frame order and slice data',
              'ranges. (', frame_order.keys(), '!=', slice_data_ranges.keys(),
              file=sys.stderr)
        return 1
    for tile_no in frame_order:
        if frame_order[tile_no].keys() != slice_data_ranges[tile_no].keys():
            print('Error: Segment number do not match for frame order and',
                  'slice data ranges for tile', tile_no, '.', file=sys.stderr)
            print(frame_order[tile_no].keys(), '!=',
                  slice_data_ranges[tile_no].keys(), file=sys.stderr)
            return 1
        for segment_no in frame_order[tile_no]:
            segment_frame_order = frame_order[tile_no][segment_no]
            segment_slice_data_ranges = slice_data_ranges[tile_no][segment_no]
            if len(segment_frame_order) != len(segment_slice_data_ranges):
                print('Error: Number of frames and slice data ranges does not',
                      'match for tile', tile_no, 'segment', segment_no, '(',
                      len(segment_frame_order), '!=',
                      len(segment_slice_data_ranges), file=sys.stderr)
                return 1
            segment_output_folder = output_path + str(tile_no) + '/' + \
                                    str(segment_no) + '/'
            os.makedirs(segment_output_folder, exist_ok=True)
            write_range_files(segment_output_folder,
                              frame_order[tile_no][segment_no],
                              slice_data_ranges[tile_no][segment_no])
    return 0


if __name__ == '__main__':
    description = """Generate range files for an increasing number of frames
    dropped. Optionally, the frames can be reordered by specyfing a separate
    weight directory. The frame directory is required to determine the position
    of the frame POCs in the original bytestream order."""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('frame_path', metavar='frame-directory',
                        help='the directory containing the frame header sizes')
    parser.add_argument('slice_data_path', metavar='slice-data-directory',
                        help='the directory containing the slice data ranges')
    parser.add_argument('output_path', metavar='output-directory',
                        help='the root output directory')

    args = parser.parse_args()
    frame_path: str = args.frame_path
    if not frame_path.endswith('/'):
        frame_path += '/'
    slice_data_path: str = args.slice_data_path
    if not slice_data_path.endswith('/'):
        slice_data_path += '/'
    output_path: str = args.output_path
    if not output_path.endswith('/'):
        output_path += '/'

    frame_order = read_frame_order(frame_path)
    if len(frame_order) == 0:
        exit(1)
    slice_data_ranges = read_slice_data_ranges(slice_data_path)
    if len(slice_data_ranges) == 0:
        exit(1)
    # TODO Read and apply weights
    status = write_output(output_path, frame_order, slice_data_ranges)
    exit(status)
