#!/bin/bash
set -euo pipefail

if [ ! $# -eq 2 ]
then
    echo "usage: $0 <path/to/interm> <path/to/output>"
    exit 1
 fi

INTERM_DIR=${1%/}
OUT_PREFIX=${2%/}

for FRAMES in "$INTERM_DIR"/*-frames
do
    PREFIX=${FRAMES%-frames}
    SLICE_DATA="$PREFIX"-slice-data
    QUALITY=$(basename "$PREFIX")
    OUT_DIR="$OUT_PREFIX"/"$QUALITY"
    mkdir -p "$OUT_DIR"
    echo "$PREFIX $OUT_DIR"
    ./generate-ordered-ranges.py "$FRAMES" "$SLICE_DATA" "$OUT_DIR"
done

