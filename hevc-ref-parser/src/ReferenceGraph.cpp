#include "ReferenceGraph.h"
#include <utility>
#include <fstream>
#include <iostream>

ReferenceGraph::ReferenceGraph(std::vector<Frame> frames_, std::vector<Reference> references_)
    : frames(std::move(frames_)), references(std::move(references_)), max_weight(0) {

}
ReferenceGraph::ReferenceGraph(const Segment &segment) : frames(segment.frames), references(segment.references), max_weight(0) {

}
void ReferenceGraph::buildWeights() {
  for (auto &reference : references) {
    total_weights[reference.poc_ref]++;
    FrameWeights &ref_frame = graph[reference.poc_ref];
    ref_frame[reference.poc]++;
    if (ref_frame[reference.poc] > max_weight) {
      max_weight = ref_frame[reference.poc];
    }
  }
}

void ReferenceGraph::printAsDat(const std::string &path) {
  std::ofstream out(path);
  if (out.fail()) {
    std::cerr << "Error: Failed to open output file " << path << "\n";
    return;
  }
  out << "frame mb_count referenced_by_frame\n";
  for (auto &frame : frames) {
    FrameWeights &weights = graph[frame.poc];
    if (weights.empty()) {
      // Create special zero line to indicate sink.
      out << frame.poc << " 0 0\n";
      continue;
    }
    for (auto &weight : weights) {
      out << frame.poc << " " << weight.second << " " << weight.first << "\n";
    }
  }
}
