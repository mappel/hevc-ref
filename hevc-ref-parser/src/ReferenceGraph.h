#ifndef HEVC_REF_PARSER__REFERENCEGRAPH_H_
#define HEVC_REF_PARSER__REFERENCEGRAPH_H_

#include <vector>
#include <cstdint>
#include <map>
#include "structs.h"

class ReferenceGraph {
 public:
  ReferenceGraph() = delete;
  ReferenceGraph(std::vector<Frame> frames_, std::vector<Reference> references_);
  explicit ReferenceGraph(const Segment &segment);
  void buildWeights();
  void printAsDat(const std::string &path);

 private:
  /**
   * Mapping for a single frame that keeps track of how many CU references from other frames exist.
   * Maps: Other frame number -> Number of references
   */
  typedef std::map<uint32_t, uint32_t> FrameWeights;
  const std::vector<Frame> frames;
  const std::vector<Reference> references;
  uint32_t max_weight;
  /// Mapping that keeps track of weights for every frame in the segment.
  std::map<uint32_t, FrameWeights> graph;
  /// Mapping that keeps track of the total weight of each frame.
  std::map<uint32_t, uint32_t> total_weights;
};

#endif //HEVC_REF_PARSER__REFERENCEGRAPH_H_
