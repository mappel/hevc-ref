#ifndef HEVC_REF_PARSER__STRUCTS_H_
#define HEVC_REF_PARSER__STRUCTS_H_

#include <cstdint>
typedef struct {
  int32_t poc;
  int32_t tile;
  char type;
  int32_t header_size; // Size in bits
} Frame;

typedef struct {
  int32_t poc;
  int32_t x;
  int32_t y;
  int32_t list;
  int32_t poc_ref; // Referenced POC
} Reference;

typedef struct {
  std::vector<Frame> frames;
  std::vector<Reference> references;
} Segment;

#endif //HEVC_REF_PARSER__STRUCTS_H_
