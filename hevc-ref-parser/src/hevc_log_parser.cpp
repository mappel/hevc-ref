#include <iostream>
#include <cstring>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <cstdint>
#include <unistd.h>
#include <vector>
#include <unordered_map>
#include <fstream>
#include <cmath>

#include "structs.h"
#include "ReferenceGraph.h"

using std::cout;
using std::cerr;

uint8_t *file_mmap;
size_t offset;
size_t file_size;

int32_t mmapFile(const std::string &path) {
  struct stat st{};
  if (stat(path.c_str(), &st) < 0) {
    cerr << "error while getting file size: " << strerror(errno) << "\n";
    return -1;
  }
  file_size = st.st_size;
  int32_t file_fd = open(path.c_str(), O_RDONLY);
  if (file_fd < 0) {
    cerr << "could not open fd: " << strerror(errno) << "\n";
    return -1;
  }
  file_mmap = static_cast<uint8_t *>(mmap(nullptr, file_size, PROT_READ, MAP_PRIVATE | MAP_POPULATE, file_fd, 0));
  if (file_mmap == MAP_FAILED) {
    close(file_fd);
    cerr << "could not mmap file: " << strerror(errno) << "\n";
    return -1;
  }
  close(file_fd);
  if (madvise(file_mmap, file_size, MADV_SEQUENTIAL) < 0) {
    cerr << "madvise: " << strerror(errno) << "\n";
  }
  return 0;
}

void seek(char to) {
  while (offset < file_size && *(file_mmap + offset) != to) {
    offset++;
  }
  offset++;
}

std::string parseString(char sep) {
  if (offset >= file_size) {
    cerr << "Error: EOF\n";
    return "EOF";
  }
  uint32_t end = 0;
  while (offset + end < file_size && *(file_mmap + offset + end) != sep && *(file_mmap + offset + end) != '\n') {
    end++;
  }
  std::string ret((char *) file_mmap + offset, end);
  offset += end + 1;
  return ret;
}

int32_t parseUnsignedInt32(char sep) {
  if (offset >= file_size) {
    cerr << "Error: EOF\n";
    return -1;
  }
  int32_t ret = 0;
  while (offset < file_size && *(file_mmap + offset) != sep && *(file_mmap + offset) != '\n') {
    if (*(file_mmap + offset) < 48 || *(file_mmap + offset) > 57) {
      cerr << "Error: Invalid number: " << *(file_mmap + offset) << "\n";
      return -1;
    }
    ret *= 10;
    ret += *(file_mmap + offset) - 48;
    offset++;
  }
  offset++;
  return ret;
}

bool parseToken(const std::string &expected, char sep) {
  std::string token = parseString(sep);
  if (token != expected) {
    cerr << "Error: Unexpected token: " << token << "\n";
    return false;
  }
  return true;
}

Frame parseFrame() {
  if (offset >= file_size) {
    cerr << "Error: EOF\n";
    return Frame{-1};
  }
  Frame ret{};
  if (!parseToken("POC", ':')) {
    return Frame{-1};
  }
  ret.poc = parseUnsignedInt32(' ');
  if (ret.poc < 0) {
    return Frame{-1};
  }
  if (!parseToken("idx", ':')) {
    return Frame{-1};
  }
  ret.tile = parseUnsignedInt32(' ');
  if (ret.tile < 0) {
    return Frame{-1};
  }
  if (!parseToken("type", ':')) {
    return Frame{-1};
  }
  if (offset >= file_size) {
    cerr << "Error: EOF\n";
    return Frame{-1};
  }
  ret.type = *(file_mmap + offset);
  offset += 2;
  return ret;
}

Reference parseReference() {
  if (offset >= file_size) {
    cerr << "Error: EOF\n";
    return Reference{-1};
  }
  Reference ret{};
  if (!parseToken("POC", ':')) {
    return Reference{-1};
  }
  ret.poc = parseUnsignedInt32(' ');
  if (ret.poc < 0) {
    return Reference{-1};
  }
  if (!parseToken("x", ':')) {
    return Reference{-1};
  }
  ret.x = parseUnsignedInt32(' ');
  if (ret.x < 0) {
    return Reference{-1};
  }
  if (!parseToken("y", ':')) {
    return Reference{-1};
  }
  ret.y = parseUnsignedInt32(' ');
  if (ret.y < 0) {
    return Reference{-1};
  }
  if (!parseToken("refPicList", ':')) {
    return Reference{-1};
  }
  ret.list = parseUnsignedInt32(' ');
  if (ret.list < 0) {
    return Reference{-1};
  }
  if (!parseToken("refPOC", ':')) {
    return Reference{-1};
  }
  ret.poc_ref = parseUnsignedInt32(' ');
  if (ret.poc_ref < 0) {
    return Reference{-1};
  }
  return ret;
}

int32_t parseFrameHeader() {
  if (offset >= file_size) {
    cerr << "Error: EOF\n";
    return -1;
  }
  if (!parseToken("bits", ':')) {
    return -1;
  }
  return parseUnsignedInt32(' ');
}

void writeFrames(const std::vector<Frame> &frames, const std::string &file) {
  std::ofstream out(file);
  if (out.fail()) {
    std::cerr << "Error: Failed to open output file " << file << "\n";
    return;
  }
  out << "frame type header_size(byte)\n";
  for (auto &frame : frames) {
    out << frame.poc << " " << frame.type << " " << ceil((double) frame.header_size / 8) << "\n";
  }
}

int main(int32_t argc, char **argv) {
  std::string references_file_prefix;
  std::string frame_headers_file_prefix;
  size_t segment_size = 0;
  std::string references_parameter = "--references";
  std::string frame_headers_parameter = "--frame-headers";
  std::string segment_size_parameter = "--segment-size";
  if (argc < 2) {
    cout << "usage: " << argv[0]
         << " <log file> [--references <path/to/references>] [--frame-headers <path/to/frames>] [--segment-size <segment-size>]\n";
    return 1;
  }
  std::string log_file_path = argv[1];
  for (int32_t i = 2; i < argc; i++) {
    std::string next_arg = argv[i];
    if (!next_arg.compare(0, next_arg.size(), references_parameter) && i + 1 < argc) {
      references_file_prefix = argv[i + 1];
      i++;
    } else if (!next_arg.compare(0, next_arg.size(), frame_headers_parameter) && i + 1 < argc) {
      frame_headers_file_prefix = argv[i + 1];
      i++;
    } else if (!next_arg.compare(0, next_arg.size(), segment_size_parameter) && i + 1 < argc) {
      segment_size = std::stoul(argv[i + 1]);
      i++;
    } else {
      cerr << "Unknown parameter or missing argument: " << argv[i] << "\n";
      return 1;
    }
  }
  file_mmap = nullptr;
  file_size = 0;

  if (mmapFile(log_file_path) < 0) {
    return 1;
  }

  // Per-tile structures.
  std::map<int32_t, std::vector<Segment>> segments;
  std::unordered_map<int32_t, std::vector<Frame>> frames;
  std::unordered_map<int32_t, std::vector<Reference>> references;
  bool first_segment = true;
  if (segment_size > 0) {
    // Fixed segment size does not need first chunk detection.
    first_segment = false;
  }
  offset = 0;
  cout << "Parsing file " << log_file_path << '\n';
  int32_t current_tile = -1;
  int32_t current_header_size = -1;
  while (offset < file_size) {
    if (*(file_mmap + offset) != '[') {
      seek('\n');
      continue;
    }
    offset++;
    if (offset >= file_size) {
      cerr << "Error: EOF\n";
      return 1;
    }
    std::string identifier = parseString(']');
    // Skip space after identifier.
    offset++;
    if (identifier == "slice") {
      Frame next = parseFrame();
      if (next.poc >= 0) {
        if ((segment_size > 0 && frames.size() == segment_size)
            || (segment_size == 0 && next.type == 'I' && next.tile == 0)) {
          // Prevent that we add an empty first segment.
          if (!first_segment) {
            for (auto const &f : frames) {
              segments[f.first].emplace_back(Segment{f.second, references[f.first]});
            }
            frames.clear();
            references.clear();
          } else {
            first_segment = false;
          }
        }
        current_tile = next.tile;
        if (current_header_size < 0) {
          cerr << "Error: Parsed frame but header size is not set yet.\n";
        } else {
          next.header_size = current_header_size;
          frames[next.tile].push_back(next);
        }
      }
    } else if (identifier == "slice-header") {
      current_header_size = parseFrameHeader();
    } else if (identifier == "ref") {
      Reference next = parseReference();
      if (next.poc >= 0) {
        references[current_tile].push_back(next);
      }
    }
  }
  // Add last chunk
  if (!frames.empty() && !references.empty()) {
    for (auto const& f : frames) {
      segments[f.first].emplace_back(Segment{f.second, references[f.first]});
    }
  }

  cout << "Parsed " << segments.size() << " tiles and " << segments[0].size() << " segments\n";

  if (munmap(file_mmap, file_size) < 0) {
    cerr << "munmap: " << strerror(errno) << "\n";
  }

  for (auto const &tile : segments) {
    cout << "Processing tile " << tile.first + 1 << "\n";
    int32_t segment_count = 1;
    for (auto const& segment : tile.second) {
      cout << "Segment " << segment_count << " (" << segment.frames.size() << " frames | " << segment.references.size()
           << " references)\n";
      ReferenceGraph graph(segment);
      graph.buildWeights();
      if (!references_file_prefix.empty()) {
        std::string tile_directory = references_file_prefix + "/" + std::to_string(tile.first + 1);
        struct stat s{};
        if (stat(tile_directory.c_str(), &s) < 0) {
          if (errno == ENOENT) {
            if (mkdir(tile_directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0) {
              cerr << "mkdir: " << tile_directory << " " << strerror(errno) << "\n";
              return 1;
            }
          } else {
            cerr << "stat: " << strerror(errno) << "\n";
            return 1;
          }
        }
        std::string suffix = "/" + std::to_string(segment_count) + ".dat";
        graph.printAsDat(tile_directory + suffix);
      }
      if (!frame_headers_file_prefix.empty()) {
        std::string tile_directory = frame_headers_file_prefix + "/" + std::to_string(tile.first + 1);
        struct stat s{};
        if (stat(tile_directory.c_str(), &s) < 0) {
          if (errno == ENOENT) {
            if (mkdir(tile_directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0) {
              cerr << "mkdir: " << tile_directory << " " << strerror(errno) << "\n";
              return 1;
            }
          } else {
            cerr << "stat: " << strerror(errno) << "\n";
            return 1;
          }
        }
        std::string suffix = "/" + std::to_string(segment_count) + ".dat";
        writeFrames(segment.frames, tile_directory + suffix);
      }
      segment_count++;
    }
  }
  return 0;
}