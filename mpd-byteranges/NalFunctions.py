NAL_UNIT_TYPES = {0: 'TRAIL_N',
                  1: 'TRAIL_R',
                  2: 'TSA_N',
                  3: 'TSA_R',
                  4: 'STSA_N',
                  5: 'STSA_R',
                  6: 'RADL_N',
                  7: 'RADL_R',
                  8: 'RASL_N',
                  9: 'RASL_R',
                  10: 'RSV_VCL_N10',
                  12: 'RSV_VCL_N12',
                  14: 'RSV_VCL_N14',
                  11: 'RSV_VCL_R11',
                  13: 'RSV_VCL_R13',
                  15: 'RSV_VCL_R15',
                  16: 'BLA_W_LP',
                  17: 'BLA_W_RADL',
                  18: 'BLA_N_LP',
                  19: 'IDR_W_RADL',
                  20: 'IDR_N_LP',
                  21: 'CRA_NUT',
                  22: 'RSV_IRAP_VCL22',
                  23: 'RSV_IRAP_VCL23',
                  24: 'RSV_VCL24',
                  25: 'RSV_VCL25',
                  26: 'RSV_VCL26',
                  27: 'RSV_VCL27',
                  28: 'RSV_VCL28',
                  29: 'RSV_VCL29',
                  30: 'RSV_VCL30',
                  31: 'RSV_VCL31',
                  32: 'VPS_NUT',
                  33: 'SPS_NUT',
                  34: 'PPS_NUT',
                  35: 'AUD_NUT',
                  36: 'EOS_NUT',
                  37: 'EOB_NUT',
                  38: 'FD_NUT',
                  39: 'PREFIX_SEI_NUT',
                  40: 'SUFFIX_SEI_NUT',
                  41: 'RSV_NVCL41',
                  42: 'RSV_NVCL42',
                  43: 'RSV_NVCL43',
                  44: 'RSV_NVCL44',
                  45: 'RSV_NVCL45',
                  46: 'RSV_NVCL46',
                  47: 'RSV_NVCL47',
                  48: 'UNSPEC48',
                  49: 'UNSPEC49',
                  50: 'UNSPEC50',
                  51: 'UNSPEC51',
                  52: 'UNSPEC52',
                  53: 'UNSPEC53',
                  54: 'UNSPEC54',
                  55: 'UNSPEC55',
                  56: 'UNSPEC56',
                  57: 'UNSPEC57',
                  58: 'UNSPEC58',
                  59: 'UNSPEC59',
                  60: 'UNSPEC60',
                  61: 'UNSPEC61',
                  62: 'UNSPEC62',
                  63: 'UNSPEC63'}


def is_slice_nal(file: bytes, offset: int) -> bool:
    header = file[offset: offset + 2]
    forbidden_zero_bit = header[0] & 0x80 >> 7
    nal_unit_type = (header[0] & 0x7E) >> 1
    nuh_layer_id = ((header[0] & 0x01) << 5) + ((header[1] & 0xF8) >> 3)
    nuh_temporal_id_plus1 = header[1] & 0x07
    # print(f'   forbidden_zero_bit : {forbidden_zero_bit:6d} {forbidden_zero_bit}')
    # print(f'        nal_unit_type : {nal_unit_type:06b} {nal_unit_type} ({NAL_UNIT_TYPES[nal_unit_type]})')
    # print(f'         nuh_layer id : {nuh_layer_id:06b} {nuh_layer_id}')
    # print(f'nuh_temporal_id_plus1 :    {nuh_temporal_id_plus1:03b} {nuh_temporal_id_plus1}')
    if forbidden_zero_bit != 0:
        raise ValueError('forbidden_zero_bit is not zero. What did you do?!')
    if (9 < nal_unit_type < 16) or nal_unit_type > 21:
        return False
    return True
