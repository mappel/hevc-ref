from collections import namedtuple
import os

Frame = namedtuple('Frame', 'poc type header_size')
FRAME_FILE_ENDING = '.dat'


class FrameDict:
    """Groups frames in a dictionary with an integer key."""
    def __init__(self):
        self.dict = dict()

    def add_frame(self, key: int, poc: int, frame_type: str, header_size: int) -> None:
        if key not in self.dict:
            self.dict[key] = [Frame(poc, frame_type, header_size)]
        else:
            self.dict[key].append(Frame(poc, frame_type, header_size))

    def get_frames(self, key: int):
        return self.dict[key]


def parse_frame_file(file: str, frames: FrameDict, key: int) -> None:
    with open(file, 'r') as f:
        # Consume headers
        f.readline()
        for line in f:
            line_split = line.split()
            if len(line_split) < 3:
                raise ValueError('Invalid frame line:' + line.strip())
            poc = int(line_split[0])
            frame_type = line_split[1]
            header_size = int(line_split[2])
            frames.add_frame(key, poc, frame_type, header_size)


def read_tile_frames(tile_path: str) -> FrameDict:
    """Read all frames for all segments of the specified tile."""
    print(tile_path)
    ret = FrameDict()
    it = os.scandir(tile_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(FRAME_FILE_ENDING):
            segment_no = int(entry.name[:-len(FRAME_FILE_ENDING)])
            parse_frame_file(tile_path + entry.name, ret, segment_no)
    return ret


def read_segment_frames(video_path: str, tile_count: int, segment_no: int) -> FrameDict:
    """Read all frames for the specified segment for the specified number of tiles."""
    ret = FrameDict()
    for tile_no in range(1, tile_count + 1):
        frame_file = video_path + str(tile_no) + '/' + str(segment_no) + FRAME_FILE_ENDING
        if not os.path.exists(frame_file):
            raise FileNotFoundError('Failed to find frame file for segment ' + str(segment_no) + ' of tile '
                                    + str(tile_no))
        parse_frame_file(frame_file, ret, tile_no)
    return ret
