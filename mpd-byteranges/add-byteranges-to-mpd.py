#!/usr/bin/python3
import argparse
import os
import re
import sys
import NalFunctions
import Frames
from Segment import Segment


def get_tile_number_from_name(name: str) -> int:
    number_re = re.compile(r'track\d+')
    match = number_re.search(name)
    if match is None:
        raise ValueError('Could not determine tile number from filename. Check filename or specify manually.')
    # MP4 track 1 is the base track, but we want to start counting tiles at 1.
    return int(match.group()[len('track'):]) - 1


def find_tile_dir(path: str, tile_no: int) -> str:
    tile_dir = path + str(tile_no) + '/'
    if not os.path.exists(tile_dir):
        raise FileNotFoundError('Failed to find tile directory: ' + tile_dir)
    return tile_dir


def parse_mdat(file: bytes, offset: int, end: int, frames: list, segment: Segment):
    frame_count = 0
    while offset < end:
        nal_start = offset
        size = int.from_bytes(file[offset: offset + 4], byteorder='big')
        offset += 4
        # nal_end_plus1 actually points to the first byte of the _next_ NAL unit.
        # Therefore we need to subtract one if we want to use it as the end of a
        # range.
        nal_end_plus1 = offset + size
        if NalFunctions.is_slice_nal(file, offset):
            # 2 bytes NAL header + slice header
            # -1 at the end since the end of the range is inclusive.
            slice_header_end = offset + 2 + frames[frame_count].header_size - 1
            if frames[frame_count].type == 'I':
                segment.add_reliable_range(nal_start, nal_end_plus1 - 1)
            else:
                segment.add_reliable_range(nal_start, slice_header_end)
                segment.add_unreliable_range(slice_header_end + 1, nal_end_plus1 - 1)
            frame_count += 1
        else:
            # Should not happen, but just in case, add reliably.
            segment.add_reliable_range(nal_start, nal_end_plus1 - 1)
        offset = nal_end_plus1


def parse_mp4(file: str, frames: Frames.FrameDict) -> list:
    with open(file, 'rb') as f:
        mp4 = f.read()
    ret = list()
    offset = 0
    segment_start = 0
    segment_no = 0
    while offset < len(mp4):
        size = int.from_bytes(mp4[offset: offset+4], byteorder='big')
        box_end_plus1 = offset + size
        offset += 4
        type = mp4[offset: offset + 4].decode(encoding='ascii')
        if type == 'mdat':
            curr_segment = Segment()
            # Range is inclusive, therefore only +3.
            curr_segment.add_reliable_range(segment_start, offset + 3)
            # Position offset at the beginning of mdat data.
            parse_mdat(mp4, offset + 4, box_end_plus1, frames.get_frames(segment_no), curr_segment)
            ret.append(curr_segment)
        elif type == 'sidx':
            segment_start = offset - 4
            segment_no += 1
        offset = box_end_plus1
    return ret


def add_ranges_to_segment_url(line: str, segment: Segment) -> str:
    reliable_size = 0
    reliable_range = 'reliable="'
    for r in segment.get_reliable_ranges():
        reliable_range += str(r) + ','
        reliable_size += r.size()
    # Replace trailing comma with quotation mark
    reliable_range = reliable_range[:-1] + '"'
    reliable_size_string = 'reliableSize="' + str(reliable_size) + '"'
    unreliable_range = 'unreliable="'
    for r in segment.get_unreliable_ranges():
        unreliable_range += str(r) + ','
    unreliable_range = unreliable_range[:-1] + '"'
    indent_size = len(line) - len(line.lstrip())
    # Because we can not be sure what the last attribute is, which will have
    # '/>' included since there is no space, we will remove it in the beginning
    # and add it in the end.
    temp_line = line.strip().rstrip('/>')
    line_split = temp_line.split()
    media_range_pos = -1
    for idx, attribute in enumerate(line_split):
        if attribute.startswith('mediaRange'):
            media_range_pos = idx + 1
            break
    if media_range_pos == -1:
        print('Warning: Failed to find mediaRange attribute. Inserting at end', file=sys.stderr)
        media_range_pos = len(line_split)
    line_split.insert(media_range_pos, reliable_size_string)
    line_split.insert(media_range_pos, unreliable_range)
    line_split.insert(media_range_pos, reliable_range)
    ret_line = ' ' * indent_size + ' '.join(line_split) + '/>\n'
    return ret_line


def write_mpd(input_file: str, output_file: str, segments: list, mp4_name: str) -> None:
    found_once = False
    base_url_found = False
    segment_count = 0
    with open(input_file, 'r') as i, open(output_file, 'w') as o:
        for line in i:
            out_line = line
            if 'BaseURL' in line:
                if mp4_name in line:
                    base_url_found = True
                    found_once = True
                else:
                    base_url_found = False
            elif 'SegmentURL' in line and base_url_found:
                out_line = add_ranges_to_segment_url(line, segments[segment_count])
                segment_count += 1
            o.write(out_line)
    if not found_once:
        raise RuntimeError('Failed to find matching BaseURL. No changes done.')


if __name__ == '__main__':
    description = """Add frame byterange information to MPD for a single tile.
                  Looks for "trackX" pattern in the MP4 file name, where X is the tile number + 1.
                  If this pattern is missing, use the -t option to manually specify the number."""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('tile', metavar='tile-track', help='the MP4 file containing the tile track')
    parser.add_argument('mpd', metavar='MPD', help='the MPD file')
    parser.add_argument('frame_path', metavar='frame-directory', help='the directory containing the frame header sizes')
    parser.add_argument('-t', '--tile', metavar='N', dest='tile_no', type=int, help='set the tile number manually')
    parser.add_argument('-o', '--output', help='set the output MPD manually')
    args = parser.parse_args()

    tile_no = args.tile_no
    if tile_no is None:
        tile_no = get_tile_number_from_name(args.tile)
    frame_path = args.frame_path
    if not frame_path.endswith('/'):
        frame_path += '/'
    output = args.output
    if output is None:
        output = os.path.splitext(args.mpd)[0] + '_output.mpd'

    tile_dir = find_tile_dir(frame_path, tile_no)
    frames = Frames.read_tile_frames(tile_dir)
    segments = parse_mp4(args.tile, frames)
    write_mpd(args.mpd, output, segments, os.path.basename(args.tile))
    exit(0)
