#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import os
import Frames
import NalFunctions
from Segment import Range


OUTPUT_FILE_ENDING = '.range'


def read_until_start_code_prefix_one_3bytes(file: bytes, offset: int, size: int):
    if offset >= size:
        print('EOF reached.')
        return size
    while (offset + 2 < size
           and not (file[offset] == 0x00 and file[offset + 1] == 0x00 and file[offset + 2] == 0x01)) \
            and \
            (offset + 3 < size
             and not (file[offset] == 0x00 and file[offset + 1] == 0x00 and file[offset + 2] == 0x00
                      and file[offset + 3] == 0x01)):
        if file[offset] != 0x00:
            raise ValueError('leading_zero_8bits are not zero.')
        offset += 1
    if offset + 2 >= size:
        raise ValueError('EOF reached where start_code_prefix_one_3bytes was expected.')
    if not (file[offset] == 0x00 and file[offset + 1] == 0x00 and file[offset + 2] == 0x01):
        if file[offset] != 0x00:
            raise ValueError('Error: zero_byte is not zero.')
        offset += 1
    if offset + 2 >= size:
        raise ValueError('EOF reached where start_code_prefix_one_3bytes was expected.')
    if not (file[offset] == 0x00 and file[offset + 1] == 0x00 and file[offset + 2] == 0x01):
        raise ValueError('Expected start_code_prefix_one_3bytes (000001) but got {:2X}{:2X}{:2X}'
              .format(*file[offset:offset + 3]))
    offset += 3
    return offset


def read_nal_unit_size(file: bytes, offset: int, size: int):
    nal_unit_size = 0
    if offset >= size:
        print('EOF reached.')
        return size, nal_unit_size
    while offset + 2 < size \
            and not (file[offset] == 0x00 and file[offset + 1] == 0x00 and file[offset + 2] == 0x00) \
            and not (file[offset] == 0x00 and file[offset + 1] == 0x00 and file[offset + 2] == 0x01):
        nal_unit_size += 1
        offset += 1
    if offset + 2 >= size:
        # There can be at max two trailing bytes if we enter this condition.
        nal_unit_size += size - offset
        offset = size
    # Implicit else: We reached one of the sequences.
    return offset, nal_unit_size


def read_trailing_zero_8bits(file: bytes, offset: int, size: int):
    while (offset + 2 < size
           and not (file[offset] == 0x00 and file[offset + 1] == 0x00 and file[offset + 2] == 0x01)) \
            and \
            (offset + 3 < size
             and not (file[offset] == 0x00 and file[offset + 1] == 0x00 and file[offset + 2] == 0x00
                      and file[offset + 3] == 0x01)):
        if file[offset] != 0x00:
            raise ValueError('trailing_zero_8bits are not zero.')
        offset += 1
    return offset


def parse_segment(file: str, frames: Frames.FrameDict, tile_count: int) -> dict:
    with open(file, 'rb') as f:
        hevc = f.read()
        hevc_size = len(hevc)
    ret = dict()
    offset = 0
    tile_no = 1
    frame_count = 0
    while offset < hevc_size:
        offset = read_until_start_code_prefix_one_3bytes(hevc, offset, hevc_size)
        nal_unit_start = offset
        slice_nal = NalFunctions.is_slice_nal(hevc, nal_unit_start)
        offset, nal_unit_size = read_nal_unit_size(hevc, offset, hevc_size)
        # Last byte of this NAL unit.
        nal_unit_end = nal_unit_start + nal_unit_size - 1
        offset = read_trailing_zero_8bits(hevc, offset, hevc_size)
        if slice_nal:
            # 2 bytes NAL header + slice header
            slice_data_start = nal_unit_start + 2 + frames.get_frames(tile_no)[frame_count].header_size
            if tile_no not in ret:
                ret[tile_no] = list()
            # Do not drop I-frames.
            if frames.get_frames(tile_no)[frame_count].type != 'I':
                ret[tile_no].append(Range(slice_data_start, nal_unit_end))
            tile_no += 1
            if tile_no > tile_count:
                tile_no = 1
                frame_count += 1
    return ret


def write_output(output_path: str, segment_no: int, slice_data: dict) -> None:
    for tile_no in slice_data:
        tile_path = output_path + str(tile_no) + '/'
        os.makedirs(tile_path, exist_ok=True)
        output_file = tile_path + str(segment_no) + OUTPUT_FILE_ENDING
        with open(output_file, 'w') as f:
            for frame_range in slice_data[tile_no]:
                f.write(frame_range.separate_by(' ') + '\n')


if __name__ == '__main__':
    description = """Parses a tiled HEVC file, which should represent a single segment, and generates one range file per
                     tile that contains all slice data byte ranges (except I-frame) of the respective tile.
                     Requires the parsed slice header sizes from the reference decoder."""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('hevc', help='the input video')
    parser.add_argument('tile_count', type=int, help='the number of tiles')
    parser.add_argument('segment_no', type=int, help='the segment number')
    parser.add_argument('frame_path', metavar='frame-directory', help='the directory containing the frame header sizes')
    parser.add_argument('output', metavar='output-directory', help='the root output directory')
    args = parser.parse_args()

    frame_path: str = args.frame_path
    if not frame_path.endswith('/'):
        frame_path += '/'
    output_path: str = args.output
    if not output_path.endswith('/'):
        output_path += '/'

    frames = Frames.read_segment_frames(frame_path, args.tile_count, args.segment_no)
    slice_data_ranges = parse_segment(args.hevc, frames, args.tile_count)
    write_output(output_path, args.segment_no, slice_data_ranges)
