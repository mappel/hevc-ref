class Range:
    def __init__(self, start: int, end: int) -> None:
        assert 0 <= start <= end
        self.start = start
        self.end = end

    def size(self) -> int:
        return self.end - self.start + 1

    def __str__(self) -> str:
        return str(self.start) + '-' + str(self.end)

    def separate_by(self, separator: str) -> str:
        return str(self.start) + separator + str(self.end)


class Segment:
    def __init__(self) -> None:
        self.reliable_ranges = list()
        self.unreliable_ranges = list()

    def __str__(self) -> str:
        ret = 'reliable:'
        for r in self.reliable_ranges:
            ret += ' ' + str(r)
        ret += '\nunreliable:'
        for r in self.unreliable_ranges:
            ret += ' ' + str(r[2])
        return ret

    def add_reliable_range(self, start: int, end: int) -> None:
        self.reliable_ranges.append(Range(start, end))

    def add_unreliable_range(self, start: int, end: int, weight: int = 0) -> None:
        self.unreliable_ranges.append((weight, start, Range(start, end)))

    def get_reliable_ranges(self):
        return self.reliable_ranges

    def get_unreliable_ranges(self):
        ret = list()
        # Sort frames by descending weight and then ascending byte location.
        for weight, start, range in sorted(self.unreliable_ranges, key=lambda e: (-e[0], e[1])):
            ret.append(range)
        return ret
