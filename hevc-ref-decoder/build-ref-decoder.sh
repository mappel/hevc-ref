#!/bin/bash
set -euxo pipefail

tar -xzf HM-HM-16.22.tar.gz
cd HM-HM-16.22
patch -p0 < ../print-information.patch
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j
